FROM alpine/git:latest AS pull_marblerun
RUN git clone https://github.com/edgelesssys/marblerun.git /marblerun

FROM ghcr.io/edgelesssys/edgelessrt-dev AS build-premain
COPY --from=pull_marblerun /marblerun /premain
WORKDIR /premain/build
RUN cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
RUN make premain-libos

FROM redis:6.2.6
RUN apt update && \
    apt install -y libssl-dev gnupg software-properties-common

RUN apt-key adv --fetch-keys https://packages.gramineproject.io/gramine.asc && \
    add-apt-repository 'deb [arch=amd64] https://packages.gramineproject.io/ stable main'

RUN apt-get update && apt-get install -y \
    wget \
    build-essential \
    libprotobuf-c-dev \
    libcurl4 \
    gramine && \
    wget https://packages.microsoft.com/ubuntu/20.04/prod/pool/main/a/az-dcap-client/az-dcap-client_1.10_amd64.deb && \
    dpkg -i az-dcap-client_1.10_amd64.deb && \
    rm az-dcap-client_1.10_amd64.deb && \
    apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y

WORKDIR /usr/local/bin
COPY enclave-key.pem /gramine/enclave-key.pem
COPY --from=build-premain /premain/build/premain-libos ./
COPY Makefile redis-server.manifest.template ./
RUN make clean && make SGX=1
ENTRYPOINT ["gramine-direct", "/usr/local/bin/redis-server" ]
